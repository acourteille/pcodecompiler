package fr.mobarzik.pcodecompiler;

import fr.mobarzik.pcodecompiler.lexer.AnalyzerException;
import fr.mobarzik.pcodecompiler.lexer.Lexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        File f = new File("/home/alexis/IdeaProjects/PcodeCompiler/src/program.txt");
        String file = "";
        try{
            Scanner sc = new Scanner(f);

            while (sc.hasNextLine()) {
                file += sc.nextLine() + "\n";
            }
        }catch(FileNotFoundException e){
            System.err.println(e.getMessage());
        }


        Lexer lexer = new Lexer(file);
        try {
            lexer.tokenize(file);
        } catch (AnalyzerException e) {
            e.printStackTrace();
        }

    }
}
